@extends('dashboard.layout')

@section('konten')
    <p class="card-title">Create experience</p>
    <div class="pd-3 mb-3"><a href="{{ route('experience.index') }}" class="btn btn-secondary">kembali</a> </div>
    <div class="table-responsive">
    <form action="{{ route ('experience.store') }}" method="POST">
        @csrf
        <div class="mb-3">
          <label for="judul" class="form-label">Posisi</label>
          <input type="input"
              class="form-control form-control-sm" name="judul" id="judul" aria-describedby="helpId" placeholder="Posisi" value="{{ Session::get('judul') }}" >
        </div>
        <div class="mb-3">
            <label for="info1" class="form-label">Nama Perusahaan</label>
            <input type="input"
                class="form-control form-control-sm" name="info1" id="info1" aria-describedby="helpId" placeholder="Nama Perusahaan" value="{{ Session::get('info1') }}" >
        </div>
        <div class="mb-3">
            <div class="row">
                <div class="col-auto">Tanggal Mulai </div>
                <div class="col-auto"><input type="date" class="form-control form-control-sm" name="tgl_mulai" placeholder="Tanggal Mulai" value="{{ Session::get('tgl_mulai') }}" > </div>
                <div class="col-auto">Tanggal Akhir </div>
                <div class="col-auto"><input type="date" class="form-control form-control-sm" name="tgl_akhir" placeholder="Tanggal Akhir" value="{{ Session::get('tgl_akhir') }}" ></div>
            </div>
        </div>
        <div class="mb-3 ">
            <label for="isi" class="form-label">Isi</label>
            <textarea class="form-control summernote " rows="5" name="isi" id="isi" >{{ Session::get('isi') }}</textarea>
          </div>

    

          <button class="btn btn-primary" name="simpan" type="submit">SIMPAN</button>
        
    
    
    </form>

    </div>
@endsection
