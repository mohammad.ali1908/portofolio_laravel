@extends('dashboard.layout')

@section('konten')
    <p class="card-title">Edit Experience</p>
    <div class="pd-3 mb-3"><a href="{{ route('experience.index') }}" class="btn btn-secondary">kembali</a> </div>
    <div class="table-responsive">
    <form action="{{ route ('experience.update',$data->id) }}" method="POST">
        @csrf
        @method('put')
        <div class="mb-3">
          <label for="judul" class="form-label">Posisi</label>
          <input type="input"
              class="form-control form-control-sm" name="judul" id="judul" aria-describedby="helpId" placeholder="Posisi" value="{{ $data->judul }}" >
        </div>
        <div class="mb-3">
            <label for="info1" class="form-label">Nama Perusahaan</label>
            <input type="input"
                class="form-control form-control-sm" name="info1" id="info1" aria-describedby="helpId" placeholder="Nama Perusahaan" value="{{ $data->info1 }}" >
        </div>
        <div class="mb-3">
            <div class="row">
                <div class="col-auto">Tanggal Mulai </div>
                <div class="col-auto"><input type="date" class="form-control form-control-sm" name="tgl_mulai" placeholder="Tanggal Mulai" value="{{  $data->tgl_mulai }}" > </div>
                <div class="col-auto">Tanggal Akhir </div>
                <div class="col-auto"><input type="date" class="form-control form-control-sm" name="tgl_akhir" placeholder="Tanggal Akhir" value="{{  $data->tgl_akhir }}" ></div>
            </div>
        </div>
        <div class="mb-3 ">
            <label for="isi" class="form-label">Isi</label>
            <textarea class="form-control summernote " rows="5" name="isi" id="isi" >{{$data->isi }}</textarea>
          </div>

    

          <button class="btn btn-primary" name="simpan" type="submit">SIMPAN</button>
        
    
    
    </form>

    </div>
@endsection
