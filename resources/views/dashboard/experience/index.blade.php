@extends('dashboard.layout')

@section('konten')
    <p class="card-title">EXPERIENCE</p>
    <div class="pd-3"><a href="{{ route('experience.create') }}" class="btn btn-primary">+ Tambah</a> </div>
    <div class="table-responsive">
        <table class="table table-srtipped">
            <thead>
                <tr>
                    <th class="col-1">No</th>
                    <th class="col-2">Posisi</th>
                    <th class="col-2">Nama Perusahaan</th>
                    <th class="col-1">Tanggal mulai</th>
                    <th class="col-1">Tanggal Akhir</th>
                    <th class="col-1">Aksi</th>
                </tr>
            </thead>
            <tbody>
                
                <?php $i =1; ?>
                @foreach ($data as $item)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $item->judul }}</td>
                    <td>{{ $item->info1 }}</td>
                    <td>{{ $item->tgl_mulai_indo }}</td>
                    <td>{{ $item->tgl_akhir_indo }}</td>
                    <td>
                        <a href="{{ route('experience.edit', $item->id)}}" class="btn btn-sm btn-warning">Edit</a>
                        <form onsubmit="return confirm('apakah akan di hapus ?')" action="{{ route('experience.destroy', $item->id)}}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-sm btn-danger" type="submit" name="submit">Del</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        </table>

    </div>
@endsection
