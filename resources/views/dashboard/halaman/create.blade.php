@extends('dashboard.layout')

@section('konten')
    <p class="card-title">Create Halaman</p>
    <div class="pd-3 mb-3"><a href="{{ route('halaman.index') }}" class="btn btn-secondary">kembali</a> </div>
    <div class="table-responsive">
    <form action="{{ route ('halaman.store') }}" method="POST">
        @csrf
        <div class="mb-3">
          <label for="judul" class="form-label">Judul</label>
          <input type="input"
              class="form-control form-control-sm" name="judul" id="judul" aria-describedby="helpId" placeholder="Judul" value="{{ Session::get('judul') }}" >
        </div>
        <div class="mb-3 ">

            <label for="isi" class="form-label">Isi</label>
            <textarea class="form-control summernote " rows="5" name="isi" id="isi" >{{ Session::get('isi') }}</textarea>
          </div>

    

          <button class="btn btn-primary" name="simpan" type="submit">SIMPAN</button>
        
    
    
    </form>

    </div>
@endsection
