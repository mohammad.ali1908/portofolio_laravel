@extends('dashboard.layout')

@section('konten')
    <p class="card-title">Edit Halaman</p>
    <div class="pd-3 mb-3"><a href="{{ route('halaman.index') }}" class="btn btn-secondary">kembali</a> </div>
    <div class="table-responsive">
    <form action="{{ route ('halaman.update',$data->id) }}" method="POST">
        @csrf
        @method('put')
        <div class="mb-3">
          <label for="judul" class="form-label">Judul</label>
          <input type="input"
              class="form-control form-control-sm" name="judul" id="judul" aria-describedby="helpId" placeholder="Judul" value="{{ $data->judul }}" >
        </div>
        <div class="mb-3 ">

            <label for="isi" class="form-label">Isi</label>
            <textarea class="form-control summernote " rows="5" name="isi" id="isi" >{{ $data->isi }}</textarea>
          </div>

    

          <button class="btn btn-primary" name="simpan" type="submit">SIMPAN</button>
        
    
    
    </form>

    </div>
@endsection
