@extends('dashboard.layout')

@section('konten')
    <p class="card-title">Education</p>
    <div class="pd-3"><a href="{{ route('education.create') }}" class="btn btn-primary">+ Tambah</a> </div>
    <div class="table-responsive">
        <table class="table table-srtipped">
            <thead>
                <tr>
                    <th class="col-1">No</th>
                    <th class="col-2">Sekolah</th>
                    <th class="col-2">Nama Perusahaan</th>
                    <th class="col-1">Jurusan</th>
                    <th class="col-1">Nama Prodi</th>
                    <th class="col-1">Tanggal mulai</th>
                    <th class="col-1">Tanggal Akhir</th>
                    <th class="col-1">Aksi</th>
                </tr>
            </thead>
            <tbody>
                
                <?php $i =1; ?>
                @foreach ($data as $item)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $item->judul }}</td>
                    <td>{{ $item->info1 }}</td>
                    <td>{{ $item->info2 }}</td>
                    <td>{{ $item->info3 }}</td>
                    <td>{{ $item->tgl_mulai_indo }}</td>
                    <td>{{ $item->tgl_akhir_indo }}</td>
                    <td>
                        <a href="{{ route('education.edit', $item->id)}}" class="btn btn-sm btn-warning">Edit</a>
                        <form onsubmit="return confirm('apakah akan di hapus ?')" action="{{ route('education.destroy', $item->id)}}" method="POST" class="d-inline">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-sm btn-danger" type="submit" name="submit">Del</button>
                        </form>
                    </td>
                </tr>
                @endforeach
                
            </tbody>
        </table>

    </div>
@endsection
