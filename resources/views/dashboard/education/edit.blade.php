@extends('dashboard.layout')

@section('konten')
    <p class="card-title">Edit education</p>
    <div class="pd-3 mb-3"><a href="{{ route('education.index') }}" class="btn btn-secondary">kembali</a> </div>
    <div class="table-responsive">
    <form action="{{ route ('education.update',$data->id) }}" method="POST">
        @csrf
        @method('put')
        <div class="mb-3">
          <label for="judul" class="form-label">Sekolah</label>
          <input type="input"
              class="form-control form-control-sm" name="judul" id="judul" aria-describedby="helpId" placeholder="Sekolah" value="{{ $data->judul }}" >
        </div>
        <div class="mb-3">
            <label for="info1" class="form-label">Jurusan</label>
            <input type="input"
                class="form-control form-control-sm" name="info1" id="info1" aria-describedby="helpId" placeholder="Jurusan" value="{{ $data->info1 }}" >
        </div>
        <div class="mb-3">
            <label for="info2" class="form-label">Nama Prodi</label>
            <input type="input"
                class="form-control form-control-sm" name="info2" id="info2" aria-describedby="helpId" placeholder="Nama Prodi" value="{{ $data->info2 }}" >
        </div>
        <div class="mb-3">
            <label for="info3" class="form-label">IPK</label>
            <input type="input"
                class="form-control form-control-sm" name="info3" id="info3" aria-describedby="helpId" placeholder="IPK" value="{{ $data->info3 }}" >
        </div>
        <div class="mb-3">
            <div class="row">
                <div class="col-auto">Tanggal Mulai </div>
                <div class="col-auto"><input type="date" class="form-control form-control-sm" name="tgl_mulai" placeholder="Tanggal Mulai" value="{{  $data->tgl_mulai }}" > </div>
                <div class="col-auto">Tanggal Akhir </div>
                <div class="col-auto"><input type="date" class="form-control form-control-sm" name="tgl_akhir" placeholder="Tanggal Akhir" value="{{  $data->tgl_akhir }}" ></div>
            </div>
        </div>
        

          <button class="btn btn-primary" name="simpan" type="submit">SIMPAN</button>
        
    
    
    </form>

    </div>
@endsection
