@extends('dashboard.layout')

@section('konten')

    <p class="card-title">Pengaturan Halaman</p>
    <div class="table-responsive">
    <form action="{{ route ('pengaturanhal.update') }}" method="POST">
        @csrf
        <div class="form-group row ">
            <label class="col-sm-2">About</label>
            <div class="col-sm-6 mb-3">
                <select class="form-control form-control-sm" name="_halaman_about">
                    <option value="">--pilih--</option>
                    @foreach ( $dataHalaman as $item )
                        <option value="{{ $item->id }}" {{ get_meta_value('_halaman_about')==$item->id?'selected' : ''}}>{{ $item->judul }}</option>
                    @endforeach

                </select>
            </div>
        </div>
        <div class="form-group row ">
            <label class="col-sm-2">Interest</label>
            <div class="col-sm-6 mb-3">
                <select class="form-control form-control-sm" name="_halaman_interest">
                    <option value="">--pilih--</option>
                    @foreach ( $dataHalaman as $item )
                        <option value="{{ $item->id }}" {{ get_meta_value('_halaman_interest')==$item->id?'selected' : ''}}>{{ $item->judul }}</option>
                    @endforeach

                </select>
            </div>
        
        </div>

          <button class="btn btn-primary" name="simpan" type="submit">SIMPAN</button>
        
    
    
    </form>



    </div>
@endsection
