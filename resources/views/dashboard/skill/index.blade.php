@extends('dashboard.layout')

@section('konten')

    <p class="card-title">Create Skill</p>
    <div class="table-responsive">
    <form action="{{ route ('skill.update') }}" method="POST">
        @csrf
        <div class="mb-3">
          <label for="_language" class="form-label">PROGRAMMING LANGUAGES & TOOLS</label>
          <input type="input"
              class="form-control form-control-sm skill" name="_language" id="judul" aria-describedby="helpId" placeholder="PROGRAMMING LANGUAGES & TOOLS" value="{{ get_meta_value('_language')}}" >
        </div>
        <div class="mb-3 ">

            <label for="isi" class="form-label">WORKFLOW</label>
            <textarea class="form-control summernote " rows="5" name="_workflow" id="isi" >{{ get_meta_value('_workflow')}}</textarea>
          </div>

          <button class="btn btn-primary" name="simpan" type="submit">SIMPAN</button>
        
    
    
    </form>

    @push('child-script')  
    <script>
        $(document).ready(function() {
            $('.skill').tokenfield({
                autocomplete: {
                    source: [{!! $skill !!}],
                    delay: 100
                },
                showAutocompleteOnFocus: true
            });
        });
    </script>
    @endpush

    </div>
@endsection
