@extends('dashboard.layout')

@section('konten')

    <p class="card-title">Create Profile</p>
    <div class="table-responsive">
    <form action="{{ route ('profile.update') }}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="row justify-content-between">
            <div class="col-5">

                @if (get_meta_value('_foto'))
                    <img style="max-width:100px;max-height:100px" src="{{ asset('foto')."/".get_meta_value('_foto') }} ">
                @endif
                <div class="mb-3">       
                    <label for="_foto" class="form-label">Foto</label>
                    <input type="file"
                        class="form-control form-control-sm " name="_foto" id="_foto" aria-describedby="helpId" placeholder="Foto" >
                     
                </div>
                <div class="mb-3">       
                    <label for="_kota" class="form-label">Kota</label>
                    <input type="input"
                        class="form-control form-control-sm " name="_kota" id="_kota" aria-describedby="helpId" placeholder="Kota" value="{{get_meta_value('_kota')}}" >
                     
                </div>
                <div class="mb-3">       
                    <label for="_propinsi" class="form-label">Propinsi</label>
                    <input type="input"
                        class="form-control form-control-sm " name="_propinsi" id="_propinsi" aria-describedby="helpId" placeholder="Propinsi" value="{{get_meta_value('_propinsi')}}" >
                     
                </div>
                <div class="mb-3">       
                    <label for="_nohp" class="form-label">No HP</label>
                    <input type="input"
                        class="form-control form-control-sm " name="_nohp" id="_nohp" aria-describedby="helpId" placeholder="No HP" value="{{get_meta_value('_nohp')}}">
                     
                </div>
                <div class="mb-3">       
                    <label for="_email" class="form-label">Email</label>
                    <input type="input"
                        class="form-control form-control-sm " name="_email" id="_email" aria-describedby="helpId" placeholder="Email" value="{{get_meta_value('_email')}}" >
                     
                </div>         
            </div>
            <div class="col-5">
                <div class="mb-3">       
                    <label for="_facebook" class="form-label">Facebook</label>
                    <input type="input"
                        class="form-control form-control-sm " name="_facebook" id="_facebook" aria-describedby="helpId" placeholder="Facebook"  value="{{get_meta_value('_facebook')}}">
                     
                </div>
                <div class="mb-3">       
                    <label for="_twitter" class="form-label">Twitter</label>
                    <input type="input"
                        class="form-control form-control-sm " name="_twitter" id="_twitter" aria-describedby="helpId" placeholder="Twitter"  value="{{get_meta_value('_twitter')}}" >
                     
                </div>
                <div class="mb-3">       
                    <label for="_LinkedIn" class="form-label">LinkedIn</label>
                    <input type="input"
                        class="form-control form-control-sm " name="_linkedin" id="_linkedin" aria-describedby="helpId" placeholder="LinkedIn"  value="{{get_meta_value('_linkedin')}}" >
                     
                </div>
                <div class="mb-3">       
                    <label for="_gitlab" class="form-label">Gitlab</label>
                    <input type="input"
                        class="form-control form-control-sm " name="_gitlab" id="_gitlab" aria-describedby="helpId" placeholder="Gitlab"  value="{{get_meta_value('_gitlab')}}">
                     
                </div>   


            </div>
        </div>
    

          <button class="btn btn-primary" name="simpan" type="submit">SIMPAN</button>
        
    
    
    </form>



    </div>
@endsection
