<?php

namespace App\Http\Controllers;

use App\Models\halaman;
use App\Models\metadata;
use Illuminate\Http\Request;

class pengaturanHalController extends Controller
{
    function index(){

        $datahalaman=halaman::orderBy('judul','asc')->get();
        return view("dashboard.pengaturanhal.index")->with('dataHalaman', $datahalaman);

    }

    function update(Request $request){
        metadata::updateOrCreate(
        ['meta_key'=>'_halaman_about'],['meta_value' =>$request->_halaman_about]
         );
         metadata::updateOrCreate(
            ['meta_key'=>'_halaman_interest'],['meta_value' =>$request->_halaman_interest]
             );
    return redirect()->route('pengaturanhal.index')->with('success','Berhasil update/create data pengaturan Halaman');
    }


}
